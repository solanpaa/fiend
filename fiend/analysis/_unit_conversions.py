"""
Conversion factors from atomic units to more common units.
"""


class conversion_factors:
    au_in_fs = 0.02418884
    au_in_eV = 27.21139
    au_in_nm = 0.0529177211
